var namespaces_dup =
[
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "Lab1FsmLightPattern", null, [
      [ "onButtonPressFCN", "Lab1FsmLightPattern_8py.html#a0500d0051af5787f41ef8ea19ac1fdc9", null ],
      [ "resetTimer", "Lab1FsmLightPattern_8py.html#a520ced572dfff1f39e77480ca33f6853", null ],
      [ "updateSqw", "Lab1FsmLightPattern_8py.html#ae16351dc32e4d070848214410ff817b5", null ],
      [ "updateStw", "Lab1FsmLightPattern_8py.html#ab6a78fbf8c10f34f86e41da50e816c79", null ],
      [ "updateSw", "Lab1FsmLightPattern_8py.html#aaae9260d6d4f152eddef71483977accb", null ],
      [ "updateTimer", "Lab1FsmLightPattern_8py.html#af63c39f92ee96b5f651d11f6043fd1ff", null ],
      [ "brightness", "Lab1FsmLightPattern_8py.html#a42e76af46e848829863867ebe305901c", null ],
      [ "elapsedTime", "Lab1FsmLightPattern_8py.html#a338219b181a0968770e00505500c023e", null ],
      [ "startTime", "Lab1FsmLightPattern_8py.html#acda9882b63a0e2c2985f431515c7e243", null ],
      [ "state", "Lab1FsmLightPattern_8py.html#a555e06f652519554c930cab96fa83062", null ]
    ] ],
    [ "main", null, [
      [ "moto", "main_8py.html#a385bbf44e78f0a990eafaf8d62b3b606", null ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ]
];