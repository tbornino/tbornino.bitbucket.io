''' @file           main.py
    @brief          Finite State Machine Skeleton Code
    @details        This file will implement a finite state machine in python.
'''

# statements go at top near docstring
import utime
import pyb
import math

#functions should be defined near the top before the main part of the program

def onButtonPressFCN(IRQ_src):
    global buttonFlag
    buttonFlag = True
    
def resetTimer():
    startTime = utime.ticks_ms()
    return startTime
    
def updateTimer(startTime):
    stopTime = utime.ticks_ms()
    elapsedTime = utime.ticks_diff(stopTime, startTime)
    return elapsedTime

def updateSqw(elapsedTime):
    return 100 * (elapsedTime % 1000 > 500)

def updateSw(elapsedTime):
    return 50 + 50 * math.sin( (2 * math.pi / 10000) * (elapsedTime % 10000))

def updateStw(elapsedTime):
    return (elapsedTime % 1000) / 10
 
    
# main program 
if __name__ == '__main__':
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                               pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    state = 0
    brightness = 0
    
    global buttonFlag
    buttonFlag = False
    welcomed = False
    
    while(True):
    
        try:
            

            if (state == 0):
                print("Initializing...")
                state = 1 # Transition to state 1
                
            elif (state == 1):
                if welcomed == False:
                    print("Push button B1 (blue) to start light show"
                          "Push again to cycle through patterns.")
                    welcomed = True
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    print("Square Wave Pattern Selected")
                    state = 2
                    continue
            elif (state == 2):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 3
                    print("Sine Wave Pattern Selected")
                    continue
                else:
                    elapsedTime = updateTimer(startTime)
                    brightness = updateSqw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)

            elif (state == 3):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 4
                    print("Sawtooth Wave Pattern Selected")
                    continue
                else:
                    elapsedTime = updateTimer(startTime)
                    brightness = updateSw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)
                    
            elif (state == 4):
                if (buttonFlag == True):
                    buttonFlag = False
                    startTime = resetTimer()
                    state = 2
                    print("Square Wave Pattern Selected")
                    continue
                else:
                    elapsedTime = updateTimer(startTime)
                    brightness = updateStw(elapsedTime)
                    t2ch1.pulse_width_percent(brightness)
                       
        except KeyboardInterrupt:
            break
            
    print('Program Termination')
                