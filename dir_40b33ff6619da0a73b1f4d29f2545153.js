var dir_40b33ff6619da0a73b1f4d29f2545153 =
[
    [ "BNO055.py", "termproject_2BNO055_8py.html", "termproject_2BNO055_8py" ],
    [ "DRV8847.py", "termproject_2DRV8847_8py.html", "termproject_2DRV8847_8py" ],
    [ "main.py", "termproject_2main_8py.html", null ],
    [ "shares.py", "termproject_2shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "termproject_2task__controller_8py.html", "termproject_2task__controller_8py" ],
    [ "task_data_collect.py", "task__data__collect_8py.html", [
      [ "task_data_collect.Task_Data_Collect", "classtask__data__collect_1_1Task__Data__Collect.html", "classtask__data__collect_1_1Task__Data__Collect" ]
    ] ],
    [ "task_imu.py", "task__imu_8py.html", "task__imu_8py" ],
    [ "task_panel.py", "task__panel_8py.html", [
      [ "task_panel.Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "task_user.py", "termproject_2task__user_8py.html", "termproject_2task__user_8py" ],
    [ "touchpanel.py", "touchpanel_8py.html", "touchpanel_8py" ]
];