var searchData=
[
  ['scanall_0',['scanAll',['../classtouchpanel_1_1TouchPanel.html#a29a98066b6a1ad26091fb542043d3cd2',1,'touchpanel::TouchPanel']]],
  ['scanallraw_1',['scanAllRaw',['../classtouchpanel_1_1TouchPanel.html#a9d502bb9c641292635b962f8cbe57c35',1,'touchpanel::TouchPanel']]],
  ['scanx_2',['scanX',['../classtouchpanel_1_1TouchPanel.html#ac3256f6034b33d11460fb3fb0e2dc0b5',1,'touchpanel::TouchPanel']]],
  ['scany_3',['scanY',['../classtouchpanel_1_1TouchPanel.html#a40e6083e3f88f2b54766bcd1088281ae',1,'touchpanel::TouchPanel']]],
  ['scanz_4',['scanZ',['../classtouchpanel_1_1TouchPanel.html#acc05188126f5843d35258125a88ad6b0',1,'touchpanel::TouchPanel']]],
  ['set_5fcalibration_5',['set_calibration',['../classtouchpanel_1_1TouchPanel.html#a257dc86aebc2c9049d2d9187e4002da8',1,'touchpanel::TouchPanel']]],
  ['set_5fcalibration_5fcoefficients_6',['set_calibration_coefficients',['../classBNO055_1_1BNO055.html#ad9524187987391789de975d5def85364',1,'BNO055.BNO055.set_calibration_coefficients(self, calibCoeff)'],['../classBNO055_1_1BNO055.html#ad9524187987391789de975d5def85364',1,'BNO055.BNO055.set_calibration_coefficients(self, calibCoeff)']]],
  ['set_5fduty_7',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847.Motor.set_duty(self, duty)'],['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847.Motor.set_duty(self, duty)']]],
  ['set_5fkp_8',['set_Kp',['../classclosedloop_1_1ClosedLoop.html#a617a88880b37c7434947936e1d3a37ce',1,'closedloop::ClosedLoop']]],
  ['set_5foperating_5fmode_9',['set_operating_mode',['../classBNO055_1_1BNO055.html#a8f40c03bdcb8894b2a789c28d089099d',1,'BNO055.BNO055.set_operating_mode(self, oprMode)'],['../classBNO055_1_1BNO055.html#a8f40c03bdcb8894b2a789c28d089099d',1,'BNO055.BNO055.set_operating_mode(self, oprMode)']]],
  ['set_5fposition_10',['set_position',['../classencoder_1_1Encoder.html#a952310a137c2ec4dcfb9556e9b51e86d',1,'encoder::Encoder']]]
];
